package tk.tachyon

import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.nio.file.Paths
import java.util.*

fun isImage(path: String): Boolean {
    val last = path.takeLast(4).toLowerCase(Locale.ROOT)
    val exts = arrayOf(".png", ".jpg", "jpeg")
    return exts.contains(last)
}

fun isSound(path: String): Boolean {
    val last = path.takeLast(4).toLowerCase(Locale.ROOT)
    val exts = arrayOf(".mp3", ".aac")
    return exts.contains(last)
}

class MessagesAdapter(
    parent: ChatActivity,
    val inflater: LayoutInflater,
    val messages: ArrayList<FullChatMessage>
) : ArrayAdapter<FullChatMessage>(parent, R.layout.own_message, messages) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = convertView ?: inflater.inflate(R.layout.own_message, null, true)
        val current = messages[position]
        val message = current.message

        val user = UsersSingleton.findUser(current.senderAddress)
        val color = user?.savedInfo?.color ?: "#FF0000FF"

        val name: TextView = row.findViewById(R.id.message_author)
        val messageBody: TextView = row.findViewById(R.id.message_body)
        val avatar: View = row.findViewById(R.id.avatar)

        if (message is TextChatMessage) {
            name.text = current.findSenderName()
            messageBody.text = message.text

            val background = avatar.background as GradientDrawable
            background.setColor(Color.parseColor(color))
        } else if (message is BinaryChatMessage) {
            name.text = current.findSenderName()
            val label = if (isImage(message.name)) {
                "Image"
            } else if (isSound(message.name)) {
                "Sound"
            } else {
                "Unknown"
            }
            val text = "$label: ${message.name}"
            messageBody.text = text

            val background = avatar.background as GradientDrawable
            background.setColor(Color.parseColor(color))
        }

        return row
    }
}

class ChatActivity : AppCompatActivity() {
    companion object {
        const val PICK_FILE_RESULT_CODE = 42
    }

    lateinit var writeText: TextView
    lateinit var sendButton: ImageButton
    lateinit var messagesAll: ListView
    lateinit var messagesAllAdapter: MessagesAdapter

    lateinit var userInfo: UserInfo
    lateinit var messages: ArrayList<FullChatMessage>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val extras = intent.extras!!
        val address = extras.getString("address")!!

        userInfo = UsersSingleton.findUser(address)!!
        userInfo.chatActivity = this
        messages = userInfo.savedInfo.messages

        writeText = findViewById(R.id.write_text)
        sendButton = findViewById(R.id.send_button)
        messagesAll = findViewById(R.id.messages_all)
        messagesAllAdapter = MessagesAdapter(this, layoutInflater, messages)
        messagesAll.adapter = messagesAllAdapter
        messagesAll.onItemClickListener = AdapterView.OnItemClickListener(this::onItemClicked)

        sendButton.setOnClickListener { this.onSend() }
    }

    fun onItemClicked(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val current = messages[position]
        val sent = Date(current.time)
        showToast("sent at: $sent", false)

        if (current.message is BinaryChatMessage) {
            val bundle = Bundle()
            val size = UsersSingleton.data.size

            val intent = if (isImage(current.message.name)) {
                UsersSingleton.data.add(current.message.bytes)
                bundle.putInt("image", size)

                val intent = Intent(this, ImageViewer::class.java)
                intent
            } else if (isSound(current.message.name)) {
                UsersSingleton.data.add(current.message.bytes)
                bundle.putInt("sound", size)
                bundle.putString("name", current.message.name)

                val intent = Intent(this, SoundViewer::class.java)
                intent
            } else {
                return
            }
            intent.putExtras(bundle)

            startActivity(intent)
        }
    }

    fun onMessageReceived(message: FullChatMessage) {
        addNewMessage(message)
    }

    fun addNewMessage(message: FullChatMessage) {
        messagesAllAdapter.notifyDataSetChanged()
    }

    fun onSend() {
        val text = writeText.text.trim().toString()
        if (text.isEmpty()) {
            return
        }

        val session = userInfo.session
        if (session != null) {
            val date = Date().time
            val innerMessage = TextChatMessage(text)
            val message = FullChatMessage(UsersSingleton.selfAddress, innerMessage, date)

            messages.add(message)
            addNewMessage(message)
            session.sendPacket(TextMessagePacket(text, date))
        }

        writeText.text = ""
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.chat_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.send_image -> {
                var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.type = "*/*"
                chooseFile = Intent.createChooser(chooseFile, "choose")
                startActivityForResult(chooseFile, PICK_FILE_RESULT_CODE)
                return true
            }
            R.id.clear_messages -> {
                userInfo.savedInfo.messages.clear()
                messagesAllAdapter.notifyDataSetChanged()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun showToast(message: String, long: Boolean = true) {
        val duration = if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
        Toast.makeText(this, message, duration).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, received: Intent?) {
        super.onActivityResult(requestCode, resultCode, received)
        if (resultCode != -1 || received == null) {
            return
        }

        when (requestCode) {
            PICK_FILE_RESULT_CODE -> {
                val data = received.data!!
//                askos(data)
                val path = Paths.get(getFileName(data)!!)
                var bytes = ByteArray(0)
                var ok = true

                val resolver = applicationContext.contentResolver
                resolver.openInputStream(data).use { stream ->
                    if (stream != null) {
                        if (stream.available() > 20 * 1024 * 1024) {
                            showToast("file too big (> 20 mb)")
                            ok = false
                            return
                        }
                        bytes = stream.readBytes() ?: return
                    }
                }
                if (!ok) {
                    return
                }

                val filename = path.fileName.toString()
                if (isImage(filename) || isSound(filename)) {
                    val packet = BinaryMessagePacket(filename, bytes, Date().time)
                    val session = userInfo.session
                    if (session != null) {
                        session.sendPacket(packet)
                    }

                    val message = BinaryChatMessage(filename, bytes)
                    val full = FullChatMessage(UsersSingleton.selfAddress, message, Date().time)
                    userInfo.savedInfo.messages.add(full)
                    addNewMessage(full)
                } else {
                    showToast("file is neither image nor sound")
                }
            }
        }
    }

    fun askos(uri: Uri) {
        val intent = Intent(this, PermissionActivity::class.java)
        intent.action = "342423423423423"
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.data = uri
        startActivity(intent)

        grantUriPermission("tk.tachyon", uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

    fun getFileName(uri: Uri): String? {
        var uri = uri
        var result: String?

        //if uri is content
        if (uri.scheme != null && uri.scheme == "content") {
            val cursor: Cursor =
                getContentResolver().query(uri, null, null, null, null)!!
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    //local filesystem
                    var index = cursor.getColumnIndex("_data")
                    if (index == -1) //google drive
                        index = cursor.getColumnIndex("_display_name")
                    result = cursor.getString(index)
                    uri = if (result != null) Uri.parse(result) else return null
                }
            } finally {
                cursor.close()
            }
        }
        result = uri.path

        //get filename + ext of path
        val cut = result!!.lastIndexOf('/')
        if (cut != -1) result = result.substring(cut + 1)
        return result
    }
}