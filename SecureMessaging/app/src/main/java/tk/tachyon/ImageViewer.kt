package tk.tachyon

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class ImageViewer : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)

        val extras = intent.extras!!
        val imageIndex = extras.getInt("image")
        val image = UsersSingleton.data[imageIndex]

        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size)

        val imageView: ImageView = findViewById(R.id.image_view)
        imageView.setImageBitmap(bitmap)
    }
}
