package tk.tachyon

import kotlinx.serialization.Serializable
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.modules.SerialModule
import kotlinx.serialization.modules.SerializersModule
import java.io.ByteArrayOutputStream
import java.util.zip.Deflater
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterOutputStream

interface ProtocolPacket

@Serializable
data class UserInformation(val name: String)

@Serializable
data class InformationRequestPacket(val selfInfo: UserInformation) : ProtocolPacket

@Serializable
data class InformationResponsePacket(val selfInfo: UserInformation) : ProtocolPacket

@Serializable
data class EncryptionRequestPacket(val publicKey: ByteArray) : ProtocolPacket

@Serializable
data class EncryptionResponsePacket(val aesKey: ByteArray) : ProtocolPacket

@Serializable
data class TextMessagePacket(val text: String, val time: Long) : ProtocolPacket

@Serializable
data class BinaryMessagePacket(val name: String, val bytes: ByteArray, val time: Long) :
    ProtocolPacket

@Serializable
data class MessageWrapper(val packet: ProtocolPacket)

fun getSerialModuleForPacket(): SerialModule {
    val serializer = SerializersModule {
        polymorphic(ProtocolPacket::class) {
            InformationRequestPacket::class with InformationRequestPacket.serializer()
            InformationResponsePacket::class with InformationResponsePacket.serializer()
            EncryptionRequestPacket::class with EncryptionRequestPacket.serializer()
            EncryptionResponsePacket::class with EncryptionResponsePacket.serializer()
            TextMessagePacket::class with TextMessagePacket.serializer()
            BinaryMessagePacket::class with BinaryMessagePacket.serializer()
        }
    }
    return serializer
}

fun serialize(packet: ProtocolPacket): ByteArray {
    val serializer = getSerialModuleForPacket()
    val cbor = Cbor(context = serializer)
    val bytes = cbor.dump(MessageWrapper.serializer(), MessageWrapper(packet))
    return bytes
}

fun deserialize(buffer: ByteArray): ProtocolPacket {
    val serializer = getSerialModuleForPacket()
    val cbor = Cbor(context = serializer)
    val packet = cbor.load(MessageWrapper.serializer(), buffer).packet
    return packet
}

fun compress(buffer: ByteArray): ByteArray {
    val deflater = Deflater(Deflater.BEST_COMPRESSION)
    val byteStream = ByteArrayOutputStream()
    val deflaterStream = DeflaterOutputStream(byteStream, deflater, true)

    deflaterStream.write(buffer)
    deflaterStream.flush()

    return byteStream.toByteArray()
}

fun decompress(buffer: ByteArray): ByteArray {
    val byteStream = ByteArrayOutputStream()
    val deflater = InflaterOutputStream(byteStream)

    deflater.write(buffer)
    deflater.flush()

    return byteStream.toByteArray()
}