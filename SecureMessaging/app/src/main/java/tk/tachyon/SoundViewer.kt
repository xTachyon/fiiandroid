package tk.tachyon

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.io.File
import java.io.IOException


class SoundViewer : AppCompatActivity() {
    lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sound_viewer)

        val extras = intent.extras!!
        val soundName = extras.getString("name")!!
        val soundIndex = extras.getInt("sound")
        val sound = UsersSingleton.data[soundIndex]

        val path = "$cacheDir/$soundName"
        File(path).writeBytes(sound)

        mediaPlayer = MediaPlayer()
        mediaPlayer.setOnPreparedListener {
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start();
            }
        }

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        try {
            mediaPlayer.setDataSource(path)
        } catch (e: IOException) {
            e.printStackTrace()
            finish()
        }
        mediaPlayer.prepareAsync()
    }

    override fun onStop() {
        super.onStop()
        mediaPlayer.stop()
    }
}
