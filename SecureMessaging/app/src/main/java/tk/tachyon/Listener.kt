package tk.tachyon

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket

fun doListen(adapter: BluetoothAdapter, callback: (BluetoothSocket) -> Unit) {
    val x = adapter.listenUsingRfcommWithServiceRecord(
        "SecureMessaging",
        BLUETOOTH_UUID
    )
    while (true) {
        val socket = x.accept()
        callback(socket)
    }
}