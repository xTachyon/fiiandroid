package tk.tachyon

import android.os.Environment
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerialModule
import kotlinx.serialization.modules.SerializersModule
import java.io.File

interface ChatMessage

@Serializable
class TextChatMessage(val text: String) : ChatMessage

@Serializable
class BinaryChatMessage(val name: String, val bytes: ByteArray) : ChatMessage

@Serializable
data class FullChatMessage(val senderAddress: String, val message: ChatMessage, val time: Long) {
    fun findSenderName(): String {
        val user = UsersSingleton.users.find { it.savedInfo.address == senderAddress }
        return user?.savedInfo?.name ?: "Me"
    }
}

@Serializable
data class UserSavedInformation(
    val name: String,
    val address: String,
    val messages: ArrayList<FullChatMessage>,
    val color: String
)

@Serializable
class UserSavedInformationList(val list: ArrayList<UserSavedInformation>)

class UserInfo(val savedInfo: UserSavedInformation) {
    var session: Session? = null
    var chatActivity: ChatActivity? = null
}

fun getSerialModuleForChatMessage(): SerialModule {
    val serializer = SerializersModule {
        polymorphic(ChatMessage::class) {
            TextChatMessage::class with TextChatMessage.serializer()
            BinaryChatMessage::class with BinaryChatMessage.serializer()
        }
    }
    return serializer
}

class UsersSingleton {
    companion object {
        var dataFile = File(Environment.getDataDirectory().absolutePath + "/data.json")
        val users = ArrayList<UserInfo>()
        var selfAddress: String = ""
        val data = ArrayList<ByteArray>()

        fun initializeUsers() {
            if (!dataFile.exists()) {
                return
            }
            try {
                val text = dataFile.readText()

                val json = Json(context = getSerialModuleForChatMessage())
                val list = json.parse(UserSavedInformationList.serializer(), text)

                for (i in list.list) {
                    users.add(UserInfo(i))
                }
            } catch (e: Exception) {
            }
        }

        fun saveUsers() {
            val list = ArrayList<UserSavedInformation>()
            for (i in users) {
                list.add(i.savedInfo)
            }

            val json = Json(context = getSerialModuleForChatMessage())
            val text = json.stringify(
                UserSavedInformationList.serializer(),
                UserSavedInformationList(list)
            )

            dataFile.writeText(text)
        }

        fun findUser(address: String): UserInfo? {
            val result = users.find { it.savedInfo.address == address }
            return result
        }
    }
}