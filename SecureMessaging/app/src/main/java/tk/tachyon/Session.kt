package tk.tachyon

import android.app.Activity
import android.bluetooth.BluetoothSocket
import android.util.Log
import java.io.*
import java.security.Key
import java.security.KeyFactory
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.spec.X509EncodedKeySpec
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec

const val RSA_MODE = "RSA/None/OAEPWithSHA1AndMGF1Padding"
const val AES_MODE = "AES/CFB8/NoPadding"

class Session(
    val context: Activity,
    val user: UserInfo,
    val socket: BluetoothSocket,
    val isInitiator: Boolean
) {
    val sendingQueue = LinkedBlockingQueue<Any>()
    val ownKeys = generateOwnKeys()
    var aesKey = generateAesKeys()

    private fun generateAesKeys(): SecretKey {
        val keyGen: KeyGenerator = KeyGenerator.getInstance("AES")
        keyGen.init(128)
        return keyGen.generateKey()
    }

    private fun generateOwnKeys(): KeyPair {
        val keyGen = KeyPairGenerator.getInstance("RSA")
        keyGen.initialize(1024) // 4096
        return keyGen.generateKeyPair()
    }

    fun sendPacket(packet: ProtocolPacket) {
        sendingQueue.put(packet)
    }

    fun onClose() {
        user.session = null
    }

    fun run() {
        val closeOnException = { callback: () -> Unit ->
            try {
                callback()
            } catch (e: java.lang.Exception) {
                Log.d("chat", e.toString())
                onClose()
            }
        }

        Thread { closeOnException(this::processingThread) }.start()
        Thread { closeOnException(this::writeThread) }.start()

        if (isInitiator) {
            sendingQueue.put(EncryptionRequestPacket(ownKeys.public.encoded))
        }
    }

    private fun processingThread() {
        var stream = socket.inputStream

        while (true) {
            val message = readOneMessage(stream)
            Log.d("chat", "received: $message")

            val enc = processEncryption(stream, message)
            if (enc == null) {
                sendToMainThread(message)
            } else {
                stream = enc
            }
        }
    }

    private fun processEncryption(
        stream: InputStream,
        packet: ProtocolPacket
    ): CipherInputStream? {
        if (packet is EncryptionRequestPacket) {
            val factory = KeyFactory.getInstance("RSA")
            val encoded = X509EncodedKeySpec(packet.publicKey)
            val rsaPublicKey = factory.generatePublic(encoded)

            var cipher = Cipher.getInstance(RSA_MODE)
            cipher.init(Cipher.WRAP_MODE, rsaPublicKey)
            val aesKeyBuffer = cipher.wrap(aesKey)

            sendingQueue.put(EncryptionResponsePacket(aesKeyBuffer))
            sendingQueue.put(aesKey)

            cipher = Cipher.getInstance(AES_MODE)
            cipher.init(Cipher.DECRYPT_MODE, aesKey, IvParameterSpec(aesKey.encoded))

            return CipherInputStream(stream, cipher)
        }
        if (packet is EncryptionResponsePacket) {
            var cipher = Cipher.getInstance(RSA_MODE)
            cipher.init(Cipher.UNWRAP_MODE, ownKeys.private)
            val aesKey = cipher.unwrap(packet.aesKey, "AES", Cipher.SECRET_KEY)

            sendingQueue.put(aesKey)

            cipher = Cipher.getInstance(AES_MODE)
            cipher.init(Cipher.DECRYPT_MODE, aesKey, IvParameterSpec(aesKey.encoded))

            sendingQueue.put(InformationRequestPacket(UserInformation("salut")))
            return CipherInputStream(stream, cipher)
        }

        return null
    }

    fun process(packet: ProtocolPacket) {
        // on ui thread
        if (packet is InformationRequestPacket) {
            sendPacket(InformationResponsePacket(UserInformation("neata")))
        } else if (packet is InformationResponsePacket) {
            //
        } else if (packet is TextMessagePacket) {
            val chatMessage = FullChatMessage(
                socket.remoteDevice.address,
                TextChatMessage(packet.text),
                Date().time
            )
            user.savedInfo.messages.add(chatMessage)
            user.chatActivity?.onMessageReceived(chatMessage)
        } else if (packet is BinaryMessagePacket) {
            val chatMessage = FullChatMessage(
                socket.remoteDevice.address,
                BinaryChatMessage(packet.name, packet.bytes),
                Date().time
            )
            user.savedInfo.messages.add(chatMessage)
            user.chatActivity?.onMessageReceived(chatMessage)
        } else {
            Log.d("chat", "No handler for ${packet.javaClass.name}")
        }
    }

    private fun sendToMainThread(packet: ProtocolPacket) {
        context.runOnUiThread {
            process(packet)
        }
    }

    private fun readOneMessage(stream: InputStream): ProtocolPacket {
        var buffer = readExact(stream, Int.SIZE_BYTES)
        val mask = DataInputStream(ByteArrayInputStream(buffer)).readInt()
        val compressed = (mask and 0b1) != 0
        val size = mask shr 1

        buffer = readExact(stream, size)
        if (compressed) {
            buffer = decompress(buffer)
        }

        val message = deserialize(buffer)
        return message
    }

    private fun readExact(stream: InputStream, size: Int): ByteArray {
        val buffer = ByteArray(size)
        var total = 0
        while (total != size) {
            val read = stream.read(buffer, total, buffer.size - total)
            if (read <= -0) {
                throw Exception("hello")
            }
            total += read
        }
        return buffer
    }

    private fun writeThread() {
        var stream = socket.outputStream!!

        while (true) {
            val message = sendingQueue.poll(1, TimeUnit.SECONDS) ?: continue

            if (message is Key) {
                val cipher = Cipher.getInstance(AES_MODE)
                cipher.init(Cipher.ENCRYPT_MODE, message, IvParameterSpec(message.encoded))
                stream = CipherOutputStream(stream, cipher)
            } else if (message is ProtocolPacket) {
                val byteStream = ByteArrayOutputStream()
                writeOne(byteStream, message)
                val bytes = byteStream.toByteArray()
                Log.d("chat", "sending: bytes=${bytes.size} $message")

                stream.write(bytes)
            }
        }
    }

    private fun writeOne(stream: OutputStream, packet: ProtocolPacket) {
        val bytesUncompressed = serialize(packet)
        val bytesCompressed = compress(bytesUncompressed)

        var (size, compressed, bytes) = if (bytesCompressed.size < bytesUncompressed.size) {
            Triple(bytesCompressed.size, 1, bytesCompressed)
        } else {
            Triple(bytesUncompressed.size, 0, bytesUncompressed)
        }
        size = (size shl 1) or compressed

        val dataStream = DataOutputStream(stream)
        dataStream.writeInt(size)
        dataStream.write(bytes)
        dataStream.flush()
    }
}