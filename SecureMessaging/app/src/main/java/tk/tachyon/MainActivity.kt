package tk.tachyon

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

val BLUETOOTH_UUID = UUID.fromString("447ac979-17e6-4fa9-9ab8-2ab89ec05049")

class ServerListAdapter(
    parent: MainActivity,
    val inflater: LayoutInflater,
    val paired: Array<BluetoothDevice>
) : ArrayAdapter<String>(parent, R.layout.persons_list, Array(paired.size) { "" }) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = inflater.inflate(R.layout.persons_list, null, true)
        val current = paired[position]

        val name: TextView = row.findViewById(R.id.name)
//        val lastMessage: TextView = row.findViewById(R.id.last_message)
        val address: TextView = row.findViewById(R.id.address)

        name.text = current.name
//        lastMessage.text = "asdasdas"
        address.text = current.address

        return row
    }
}

fun getRandomColor(): String {
    val r = Random.nextInt(256)
    val g = Random.nextInt(256)
    val b = Random.nextInt(256)

    val color = String.format("#%02x%02x%02x", r, g, b)
    Log.d("chat", "generated color=$color")
    return color
}

class MainActivity : AppCompatActivity() {
    lateinit var serverList: ListView
    lateinit var paired: Array<BluetoothDevice>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        UsersSingleton.dataFile = File(filesDir.absolutePath + "/data.json")
        UsersSingleton.initializeUsers()

        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        paired = bluetoothAdapter.bondedDevices.toTypedArray()
        UsersSingleton.selfAddress = bluetoothAdapter.address

        serverList = findViewById(R.id.server_list)
        serverList.adapter = ServerListAdapter(this, layoutInflater, paired)
        serverList.onItemClickListener = AdapterView.OnItemClickListener(this::onItemClicked)

        askPermission()

        Thread(this::listen).start()
    }

    fun askPermission() {
        val READ_EXTERNAL_STORAGE_MY_CODE = 345

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_EXTERNAL_STORAGE_MY_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        val x = 5
    }

    fun listen() {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        doListen(bluetoothAdapter) {
            runOnUiThread {
                afterListenConnected(it)
            }
        }
    }

    fun afterListenConnected(socket: BluetoothSocket) {
        val device = socket.remoteDevice
        val address = device.address

        val users = UsersSingleton.users
        val found = users.find { it.savedInfo.address == address }
        if (found == null) {
            val saved =
                UserSavedInformation(device.name, device.address, ArrayList(), getRandomColor())
            val user = UserInfo(saved)
            val session = Session(this, user, socket, false)
            session.run()
            user.session = session
            users.add(user)
        } else {
            val session = Session(this, found, socket, false)
            session.run()
            found.session = session
        }
    }

    override fun onStop() {
        super.onStop()
        UsersSingleton.saveUsers()
    }

    override fun onPause() {
        super.onPause()
        UsersSingleton.saveUsers()
    }

    fun onItemClicked(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val current = paired[position]
        var user = UsersSingleton.users.find { it.savedInfo.address == current.address }
        if (user == null) {
            UsersSingleton.users.add(
                UserInfo(
                    UserSavedInformation(
                        current.name,
                        current.address,
                        ArrayList(),
                        getRandomColor()
                    )
                )
            )
        }
        user = UsersSingleton.users.find { it.savedInfo.address == current.address }!!
        startChatActivity(user)

        if (user.session == null) {
            Thread { tryConnect(current) }.start()
        }
    }

    fun tryConnect(device: BluetoothDevice) {
        var socket: BluetoothSocket?
        try {
            socket = device.createRfcommSocketToServiceRecord(BLUETOOTH_UUID)
            socket.connect()
        } catch (e: IOException) {
            socket = null
        }
        runOnUiThread {
            afterSocketConnected(device, socket)
        }
    }

    fun afterSocketConnected(device: BluetoothDevice, socket: BluetoothSocket?) {
        if (socket == null) {
            Toast.makeText(this, "can't connect to ${device.address}", Toast.LENGTH_LONG).show()
            return
        }
        val user =
            UsersSingleton.users.find { it.savedInfo.address == socket.remoteDevice.address }!!
        val session = Session(this, user, socket, true)
        session.run()
        user.session = session

        startChatActivity(user)
    }

    fun startChatActivity(user: UserInfo) {
        val bundle = Bundle()
        bundle.putString("name", user.savedInfo.name)
        bundle.putString("address", user.savedInfo.address)

        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtras(bundle)

        startActivity(intent)
    }
}
