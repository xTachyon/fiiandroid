package tk.tachyon.onlineshop

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageState
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.ByteArrayOutputStream
import java.io.File


class CameraActivity : AppCompatActivity(), SurfaceHolder.Callback {
    lateinit var surface: SurfaceView
    lateinit var camera: Camera
    lateinit var button: Button
    lateinit var surfaceHolder: SurfaceHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        surface = findViewById(R.id.surfaceView)
        button = findViewById(R.id.cameraButton)

        surfaceHolder = surfaceView.holder
        surfaceHolder.addCallback(this)

        button.setOnClickListener(this::onClick)

    }

    fun onPictureTaken(data: ByteArray, camera: Camera) {
        val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
        val bitmap =
            Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, null, true)

        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)

        val pathFolder = externalMediaDirs[0].absolutePath + "/malware/"
        File(pathFolder).mkdirs()
        val path = pathFolder + System.currentTimeMillis() + ".jpg"
        File(path).writeBytes(stream.toByteArray())

        Toast.makeText(applicationContext, "jerry cheese !", Toast.LENGTH_LONG).show()

        camera.startPreview()
    }

    fun onClick(view: View) {
        camera.takePicture(null, null, this::onPictureTaken)
    }

    override fun surfaceChanged(surface: SurfaceHolder, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(surface: SurfaceHolder) {
    }

    override fun surfaceCreated(surface: SurfaceHolder) {
        camera = Camera.open()

        val parameters = camera.getParameters()
        with (parameters) {
            setPreviewFrameRate(20);
            setPreviewSize(352, 288);
        }

        camera.setParameters(parameters);
        camera.setDisplayOrientation(90);
        camera.setPreviewDisplay(surfaceHolder);
        camera.startPreview()
    }
}
