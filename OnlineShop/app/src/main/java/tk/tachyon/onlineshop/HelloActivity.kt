package tk.tachyon.onlineshop

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        textView = findViewById(R.id.textViewFromOtherAct)

        val text = "Sent: " + intent.getStringExtra(Intent.EXTRA_TEXT)

        textView.text = text
    }
}
