package tk.tachyon.onlineshop

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class MyLocationListener(val activity: SensortActivity) : LocationListener {
    override fun onLocationChanged(location: Location) {
        val text = "latitude=${location.latitude} longitude=${location.longitude} altitude=${location.altitude}"
        activity.gpsTextView.text = text
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }
}

class SensortActivity : AppCompatActivity(), SensorEventListener {
    lateinit var listView: ListView
    lateinit var gpsTextView: TextView

    val values = HashMap<String, String>()
    var keysArray = values.keys.toTypedArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensort)

        listView = findViewById(R.id.sensorsListView)
        gpsTextView = findViewById(R.id.gpsTextView)

        setSensors()
        doGps()
    }

    fun doGps() {
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_COARSE
        val listener = MyLocationListener(this)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            val GPS = 5435
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_CONTACTS),
                GPS)
            return
        } else {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.0f, listener)
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.0f, listener)
        }
    }

    fun setSensors() {
        val manager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        for (sensor in manager.getSensorList(Sensor.TYPE_ALL)) {
            manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        reloadData(event.sensor.name, event.values!!.contentToString())
    }

    fun reloadData(key: String, value: String) {
        val exists = values.containsKey(key)
        values[key] = value

        if (!exists) {
            keysArray = values.keys.toTypedArray()
            listView.adapter = ArrayAdapter(
                this,
                android.R.layout.simple_expandable_list_item_1,
                keysArray
            )
            listView.onItemClickListener = AdapterView.OnItemClickListener(this::onListViewClicked)
        }
    }

    private fun onListViewClicked(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        Toast.makeText(this, values[keysArray[position]], Toast.LENGTH_SHORT).show()
    }
}