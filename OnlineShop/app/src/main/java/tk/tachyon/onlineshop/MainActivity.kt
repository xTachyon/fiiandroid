package tk.tachyon.onlineshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.prefs.Preferences

val MAP_SAVE_KEY = "salut"
val TEXT_VIEW_SAVE_KEY = "salut2"

class MainActivity : AppCompatActivity() {
    lateinit var listView: ListView
    lateinit var arrayAdapter: ArrayAdapter<String>
    lateinit var button: Button
    lateinit var textView: TextView

    var savedInstanceState: Bundle? = null
    var productMap = createProductMap()
    var productKeys = productMap.keys.toTypedArray()

    private fun createProductMap(): HashMap<String, String> {
        val map = HashMap<String, String>()
        map["abc"] = "abcdefghijklmnopqrstuvwxyz"
        map["123"] = "123456789"
        return map
    }

    private fun loadData(savedInstanceState: Bundle?) {
        textView.text = savedInstanceState?.getString(TEXT_VIEW_SAVE_KEY)
        val string = savedInstanceState?.getString(MAP_SAVE_KEY) ?: return

        val mapper = ObjectMapper()
        val type = object : TypeReference<HashMap<String, String>>() {}
        productMap = mapper.readValue(string, type)
    }

    private fun saveData(savedInstanceState: Bundle?) {
        val mapper = ObjectMapper()
        val string = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(productMap)
        savedInstanceState?.putString(MAP_SAVE_KEY, string)
        savedInstanceState?.putString(TEXT_VIEW_SAVE_KEY, textView.text.toString())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.listView)
        textView = findViewById(R.id.textView)

        button = findViewById(R.id.button)
        button.setOnClickListener(this::onButtonClick)

        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, productKeys)
        listView.adapter = arrayAdapter
        listView.onItemClickListener = AdapterView.OnItemClickListener(this::onListViewClicked)

        Log.d("lifecycle", "onCreate")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.hello -> {
                val intent = Intent(this, HelloActivity::class.java)
                intent.putExtra(Intent.EXTRA_TEXT, textView.text)
                startActivity(intent)

                true
            }
            R.id.dialog_item -> {
                val fragment = TimePickerDialogFragment()
                fragment.show(supportFragmentManager, "meow")

                true
            }
            R.id.preferences -> {
                val intent = Intent(this, PreferencesActivity::class.java)
                startActivity(intent)

                true
            }
            R.id.camera_item -> {
                val intent = Intent(this, CameraActivity::class.java)
                startActivity(intent)

                true
            }
            R.id.sensors_items -> {
                val intent = Intent(this, SensortActivity::class.java)
                startActivity(intent)

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("lifecycle", "onCreate")
    }

    override fun onResume() {
        super.onResume()
        Log.d("lifecycle", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("lifecycle", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("lifecycle", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("lifecycle", "onDestroy")
    }

    fun updateListView() {
        productKeys = productMap.keys.toTypedArray()

        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, productKeys)
        listView.adapter = arrayAdapter
        listView.onItemClickListener = AdapterView.OnItemClickListener(this::onListViewClicked)
    }

    fun onButtonClick(view: View) {
        productMap["new"] = "neeeeeeeeeeeww";
        updateListView()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        loadData(savedInstanceState)
        updateListView()
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        saveData(outState)
    }

    private fun onListViewClicked(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val key = productKeys[position]
        val description = productMap[key]
        textView.text = description
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menuuu, menu)
        return true
    }
}
