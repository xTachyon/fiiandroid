package tk.tachyon.onlineshop

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

val PREFERENCES_KEY = "rupe vioara tata"

data class PreferencesData(val color: String = "", val theme: String = "")

class PreferencesActivity : AppCompatActivity() {
    lateinit var color: EditText
    lateinit var theme: EditText
    lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preferences)

        color = findViewById(R.id.preferedColor)
        theme = findViewById(R.id.theme)
        button = findViewById(R.id.preferencesButton)

        button.setOnClickListener(this::onButtonClick)

        loadData()
    }

    fun onButtonClick(view: View) {
        saveData()
    }

    fun loadData() {
        var filePref = PreferencesData("red", "dark")

        val filePath = filesDir.absolutePath + "/settings.txt"
        if (Files.exists(Paths.get(filePath))) {
            val filePrefText = File(filePath).readText()
            val type = object : TypeReference<PreferencesData>() {}
            filePref = ObjectMapper().readValue(filePrefText, type)
        }

        val shared = getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)
        color.setText(shared.getString("color", filePref.color))
        theme.setText(shared.getString("theme", filePref.theme))
    }

    fun saveData() {
        val current = PreferencesData(color.text.toString(), theme.text.toString())

        val shared = getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)

        with (shared.edit()) {
            putString("color", current.color)
            putString("theme", current.theme)

            commit()
        }

        val mapper = ObjectMapper()
        val string = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(current)

        val settingsFile = filesDir.absolutePath + "/settings.txt"
        File(settingsFile).writeText(string)
    }
}
